USE [master]
GO
/****** Object:  Database [pruebacarvajal]    Script Date: 23/04/2022 11:25:22 a. m. ******/
CREATE DATABASE [pruebacarvajal]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'pruebacarvajal', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\pruebacarvajal.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'pruebacarvajal_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\pruebacarvajal_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [pruebacarvajal] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [pruebacarvajal].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [pruebacarvajal] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [pruebacarvajal] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [pruebacarvajal] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [pruebacarvajal] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [pruebacarvajal] SET ARITHABORT OFF 
GO
ALTER DATABASE [pruebacarvajal] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [pruebacarvajal] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [pruebacarvajal] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [pruebacarvajal] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [pruebacarvajal] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [pruebacarvajal] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [pruebacarvajal] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [pruebacarvajal] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [pruebacarvajal] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [pruebacarvajal] SET  DISABLE_BROKER 
GO
ALTER DATABASE [pruebacarvajal] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [pruebacarvajal] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [pruebacarvajal] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [pruebacarvajal] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [pruebacarvajal] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [pruebacarvajal] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [pruebacarvajal] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [pruebacarvajal] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [pruebacarvajal] SET  MULTI_USER 
GO
ALTER DATABASE [pruebacarvajal] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [pruebacarvajal] SET DB_CHAINING OFF 
GO
ALTER DATABASE [pruebacarvajal] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [pruebacarvajal] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [pruebacarvajal] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [pruebacarvajal] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [pruebacarvajal] SET QUERY_STORE = OFF
GO
USE [pruebacarvajal]
GO
/****** Object:  Table [dbo].[user]    Script Date: 23/04/2022 11:25:22 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[id] [int] NOT NULL,
	[email] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[vuelo]    Script Date: 23/04/2022 11:25:22 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vuelo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ciudadorigen] [varchar](50) NULL,
	[ciudaddestino] [varchar](50) NULL,
	[fecha] [date] NULL,
	[horasalida] [time](7) NULL,
	[horallegada] [time](7) NULL,
	[novuelo] [varchar](10) NULL,
	[aerolinea] [varchar](20) NULL,
	[estado] [varchar](20) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[user] ([id], [email], [password]) VALUES (1, N'admin@carvajal.com', N'123123')
INSERT [dbo].[user] ([id], [email], [password]) VALUES (2, N'pruebas@carvajal.com', N'123123')
GO
SET IDENTITY_INSERT [dbo].[vuelo] ON 

INSERT [dbo].[vuelo] ([id], [ciudadorigen], [ciudaddestino], [fecha], [horasalida], [horallegada], [novuelo], [aerolinea], [estado]) VALUES (1, N'Bogotá', N'Medellín', CAST(N'2021-10-10' AS Date), CAST(N'06:30:00' AS Time), CAST(N'07:00:00' AS Time), N'BOGMED01', N'Avianca', N'Confirmado')
INSERT [dbo].[vuelo] ([id], [ciudadorigen], [ciudaddestino], [fecha], [horasalida], [horallegada], [novuelo], [aerolinea], [estado]) VALUES (2, N'Bogotá', N'Manizales', CAST(N'2021-10-10' AS Date), CAST(N'08:00:00' AS Time), CAST(N'08:28:00' AS Time), N'BOGMAN01', N'Latam', N'Pendiente')
SET IDENTITY_INSERT [dbo].[vuelo] OFF
GO
USE [master]
GO
ALTER DATABASE [pruebacarvajal] SET  READ_WRITE 
GO
