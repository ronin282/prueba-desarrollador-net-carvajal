﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfPruebaCarvajal.Models
{
    public class Vuelo
    {
        public string ciudadOrigen { get; set; }
        public string ciudadDestino { get; set; }
        public string fecha { get; set; }
        public string horaSalida { get; set; }
        public string horaLlegada { get; set; }
        public string numeroVuelo { get; set; }
        public string aerolinea { get; set; }
        public string estado { get; set; }

    }
}
