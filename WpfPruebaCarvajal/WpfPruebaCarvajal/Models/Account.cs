﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfPruebaCarvajal.Models
{
    public class Account
    {
        public string Email { get; set; }
        public string Username { get; set; }
    }
}
