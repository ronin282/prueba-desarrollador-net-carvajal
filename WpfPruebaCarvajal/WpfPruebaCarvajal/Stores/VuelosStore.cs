﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfPruebaCarvajal.Stores
{
    public class VuelosStore
    {
        public event Action<string> VueloAdded;

        public void AddVuelo(string name)
        {
            VueloAdded?.Invoke(name);
        }
    }
}
