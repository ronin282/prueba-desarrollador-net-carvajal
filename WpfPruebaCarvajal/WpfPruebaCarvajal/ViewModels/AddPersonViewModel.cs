﻿using WpfPruebaCarvajal.Commands;
using WpfPruebaCarvajal.Services;
using WpfPruebaCarvajal.Stores;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace WpfPruebaCarvajal.ViewModels
{
    public class AddPersonViewModel : ViewModelBase
    {
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

    }
}
