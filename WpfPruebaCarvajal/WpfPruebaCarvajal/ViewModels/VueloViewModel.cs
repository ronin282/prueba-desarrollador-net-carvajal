﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfPruebaCarvajal.Models;

namespace WpfPruebaCarvajal.ViewModels
{
    public class VueloViewModel : ViewModelBase
    {
        private string ciudadOrigen;
        public string CiudadOrigen
        {
            get { return ciudadOrigen; }
            set
            {
                ciudadOrigen = value;
                OnPropertyChanged(nameof(CiudadOrigen));
            }
        }
        private string ciudadDestino;
        public string CiudadDestino
        {
            get { return ciudadDestino; }
            set
            {
                ciudadDestino = value;
                OnPropertyChanged(nameof(CiudadDestino));
            }
        }
        private string fecha;
        public string Fecha
        {
            get { return fecha; }
            set
            {
                fecha = value;
                OnPropertyChanged(nameof(Fecha));
            }
        }
        private string horaSalida;
        public string HoraSalida
        {
            get { return horaSalida; }
            set
            {
                horaSalida = value;
                OnPropertyChanged(nameof(HoraSalida));
            }
        }
        private string horaLlegada;
        public string HoraLlegada
        {
            get { return horaLlegada; }
            set
            {
                horaLlegada = value;
                OnPropertyChanged(nameof(HoraLlegada));
            }
        }
        private string numeroVuelo;
        public string NumeroVuelo
        {
            get { return numeroVuelo; }
            set
            {
                numeroVuelo = value;
                OnPropertyChanged(nameof(NumeroVuelo));
            }
        }
        private string aerolinea;
        public string Aerolinea
        {
            get { return aerolinea; }
            set
            {
                aerolinea = value;
                OnPropertyChanged(nameof(Aerolinea));
            }
        }
        private string estado;
        public string Estado
        {
            get { return estado; }
            set
            {
                estado = value;
                OnPropertyChanged(nameof(Estado));
            }
        }

        
        public VueloViewModel()
        {

        }
    }
}
