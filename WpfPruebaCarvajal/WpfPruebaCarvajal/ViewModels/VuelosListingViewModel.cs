﻿using WpfPruebaCarvajal.Commands;
using WpfPruebaCarvajal.Services;
using WpfPruebaCarvajal.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using System.Data.SqlClient;
using System.Data;
using System.Windows;
using System.Diagnostics;
using WpfPruebaCarvajal.Data;

namespace WpfPruebaCarvajal.ViewModels
{
    public class VuelosListingViewModel : ViewModelBase
    {
        private readonly VuelosStore _vuelosStore;

        private readonly ObservableCollection<VueloViewModel> _vuelos;

        public IEnumerable<VueloViewModel> Vuelos => _vuelos;

        public ICommand AddVueloCommand { get; }

        public VuelosListingViewModel(VuelosStore vuelosStore, INavigationService addVueloNavigationService)
        {
            _vuelosStore = vuelosStore;

            AddVueloCommand = new NavigateCommand(addVueloNavigationService);
            _vuelos = new ObservableCollection<VueloViewModel>();

            // Conexion SQL Server
            try
            {
                DataAccess da = new DataAccess();
                DataSet DataSetResult = da.SelectAll("vuelo");

                if (DataSetResult.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DataSetResult.Tables[0].Rows.Count; i++)
                    {   
                        _vuelos.Add(new VueloViewModel
                        {
                            CiudadOrigen = DataSetResult.Tables[0].Rows[i]["ciudadorigen"].ToString(),
                            CiudadDestino = DataSetResult.Tables[0].Rows[i]["ciudaddestino"].ToString(),
                            Fecha = DataSetResult.Tables[0].Rows[i]["fecha"].ToString(),
                            HoraSalida = DataSetResult.Tables[0].Rows[i]["horasalida"].ToString(),
                            HoraLlegada = DataSetResult.Tables[0].Rows[i]["horallegada"].ToString(),
                            NumeroVuelo = DataSetResult.Tables[0].Rows[i]["novuelo"].ToString(),
                            Aerolinea = DataSetResult.Tables[0].Rows[i]["aerolinea"].ToString(),
                            Estado = DataSetResult.Tables[0].Rows[i]["estado"].ToString(),
                        });
                    }
                }
                else
                {
                    //errormessage.Text = "Sorry! Please enter existing emailid/password.";
                }
                
            }
            catch (Exception e)
            {
                string messageBoxText = "Error en la conexión: " + e.Message;
                string caption = "Error";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Warning;
                MessageBoxResult result;
                result = MessageBox.Show(messageBoxText, caption, button, icon, MessageBoxResult.Yes);
            }
            
            _vuelosStore.VueloAdded += OnVueloAdded;
        }

        private void OnVueloAdded(string ciudadOrigen)
        {
            _vuelos.Add(new VueloViewModel
            {
                CiudadOrigen = ciudadOrigen
            });
        }
    }
}
