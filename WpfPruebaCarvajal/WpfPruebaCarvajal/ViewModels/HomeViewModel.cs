﻿using WpfPruebaCarvajal.Commands;
using WpfPruebaCarvajal.Services;
using WpfPruebaCarvajal.Stores;
using System.Windows.Input;

namespace WpfPruebaCarvajal.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {
        public string WelcomeMessage => "Esta prueba es desarrollada como producto para la convocatoria de desarrollador .net";

        public ICommand NavigateLoginCommand { get; }

        public HomeViewModel(INavigationService loginNavigationService)
        {
            NavigateLoginCommand = new NavigateCommand(loginNavigationService);
        }
    }
}
