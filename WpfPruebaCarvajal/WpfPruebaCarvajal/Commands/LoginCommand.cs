﻿using WpfPruebaCarvajal.Models;
using WpfPruebaCarvajal.Services;
using WpfPruebaCarvajal.Stores;
using WpfPruebaCarvajal.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using WpfPruebaCarvajal.Data;
using System.Data;
using System.Diagnostics;

namespace WpfPruebaCarvajal.Commands
{
    public class LoginCommand : CommandBase
    {
        private readonly LoginViewModel _viewModel;
        private readonly AccountStore _accountStore;
        private readonly INavigationService _navigationService;

        public LoginCommand(LoginViewModel viewModel, AccountStore accountStore, INavigationService navigationService)
        {
            _viewModel = viewModel;
            _accountStore = accountStore;
            _navigationService = navigationService;
        }

        public override void Execute(object parameter)
        {
            DataAccess da = new DataAccess();
            var DataSetResult = da.LoginValidate(_viewModel.Username, _viewModel.Password);
            
            if (DataSetResult.Tables[0].Rows.Count > 0)
            {
                Account account = new Account()
                {
                    Email = _viewModel.Username,
                    Username = _viewModel.Username
                };

                _accountStore.CurrentAccount = account;

                _navigationService.Navigate();
            }
            else {
                string messageBoxText = "El email o password no coinciden";
                string caption = "Advertencia";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Warning;
                MessageBoxResult result;
                result = MessageBox.Show(messageBoxText, caption, button, icon, MessageBoxResult.Yes);
            }


                
        }
    }
}
