﻿using WpfPruebaCarvajal.Services;
using WpfPruebaCarvajal.Stores;
using WpfPruebaCarvajal.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace WpfPruebaCarvajal.Commands
{
    public class AddVueloCommand : CommandBase
    {
        private readonly AddVueloViewModel _addVueloViewModel;
        private readonly VuelosStore _vuelosStore;
        private readonly INavigationService _navigationService;

        public AddVueloCommand(AddVueloViewModel addVueloViewModel, VuelosStore vuelosStore, INavigationService navigationService)
        {
            _addVueloViewModel = addVueloViewModel;
            _vuelosStore = vuelosStore;
            _navigationService = navigationService;
        }

        public override void Execute(object parameter)
        {
            string name = _addVueloViewModel.Name;
            _vuelosStore.AddVuelo(name);

            _navigationService.Navigate();
        }
    }
}
