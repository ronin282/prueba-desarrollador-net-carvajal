﻿using WpfPruebaCarvajal.ViewModels;

namespace WpfPruebaCarvajal.Services
{
    public interface INavigationService
    {
        void Navigate();
    }
}